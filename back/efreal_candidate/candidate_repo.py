class CandidateRepository:

    def __init__(self, database):
        self.db = database

    def retrieve_all_advert(self):
        """
        Lists all the adverts
        """
        with self.db.cursor() as cursor:
            query = """
                SELECT * FROM Annonce 
            """
            cursor.execute(query, ())

            return cursor.fetchall()

    def retrieve_advert(self, id_advert: int):
        """
        Retrieves the details of an advert
        """
        with self.db.cursor() as cursor:
            query = """
                SELECT Id, TypeAnnonce, Prix, Image, Ville, Description, Adresse, NombrePieces, SurfaceHabitable, Etat, IdProprietaire
                FROM Annonce WHERE Id = %s
            """
            cursor.execute(query, (id_advert,))

            return cursor.fetchone()

    def create_request(self, id_advert: int, user_id: str):
        with self.db.cursor() as cursor:
            query = """
                INSERT INTO Candidature(IdAnnonce,IdProspect,Statut) VALUES (%s,%s,%s)
            """
            # When a candidate starts a request, the status of his/her application is set to 'pending'
            cursor.execute(query, (id_advert, user_id, "Pending"))
            self.db.commit()
            return cursor.lastrowid

    def create_initial_message(self, request_id, message):
        is_owner = 0
        with self.db.cursor() as cursor:
            query = """
                INSERT INTO Message(IdCandidature, IsOwner, SentDate, Message)
                VALUES (%s, %s, NOW(), %s)
            """
            cursor.execute(query, (request_id, is_owner, message))
            self.db.commit()
