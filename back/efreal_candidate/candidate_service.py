from efreal_candidate.candidate_repo import CandidateRepository
from efreal_candidate.exceptions import CandidateException


class CandidateService:
    def __init__(self, repo: CandidateRepository):
        self.repo = repo

    def get_all_advert(self):
        """
         Récupère les informations de toutes les annonces
        """
        try:
            results = self.repo.retrieve_all_advert()
            advert_list = []
            for result in results:
                advert_details = {
                    "Id": result[0],
                    "TypeAnnonce": result[1],
                    "Prix": result[2],
                    "Image": result[3],
                    "Ville": result[4],
                    "Description": result[5]
                }
                advert_list.append(advert_details)
            return advert_list
        except Exception as e:
            raise CandidateException(
                f"An error occurred while getting advertisement information: {str(e)}")

    def get_advert(self, id_advert: int):
        """
        Récupère les informations d'une annonce
        """
        try:
            result = self.repo.retrieve_advert(id_advert)
            if result:
                advert_details = {
                    "Id": result[0],
                    "TypeAnnonce": result[1],
                    "Prix": result[2],
                    "Image": result[3],
                    "Ville": result[4],
                    "Description": result[5],
                    "Adresse": result[6],
                    "NombrePieces": result[7],
                    "SurfaceHabitable": result[8],
                    "Etat": result[9],
                    "IdProprietaire": result[10]
                }
                return advert_details
            else:
                return None

        except Exception as e:
            raise CandidateException(
                f"An error occured while getting advertisement infomations {str(e)}")

    def create_request(self, id_advert: int, message: str, user_id: str):
        """
        Crée une candidature (lie un id annonce à un id utilisateur)
        Initialise un premier message
        """
        try:
            request_id = self.repo.create_request(id_advert, user_id)
            self.repo.create_initial_message(request_id, message)
            return request_id
        except Exception as e:
            raise CandidateException(
                f"An error occured while creating a request {str(e)}")
