from efreal_candidate.candidate_repo import CandidateRepository
from efreal_candidate.candidate_service import CandidateService
from efreal_candidate.exceptions import CandidateException
from flask import jsonify, request
from efreal_shared.main import createMicroservice, getJwtData, startMicroservice, useMySQL

app = createMicroservice('candidate', __name__)
db = useMySQL()

repo = CandidateRepository(db)
service = CandidateService(repo)

@app.route('/houses', methods=['GET'])
def get_all_annonce():
    """
    Récupère toutes les annonces où le candidat a déposé une candidature
    """
    try:
        advert_details = service.get_all_advert()
        if advert_details:
            return jsonify(advert_details)
        else:
            return jsonify([])
    except CandidateException as e:
        return jsonify({"error": str(e)}), 500


@app.route('/houses/<int:idAnnonce>')
def get_annonce(idAnnonce: int):
    """
    Affiche les détails d'une unique annonce
    """
    try:
        advert_details = service.get_advert(idAnnonce)
        if advert_details:
            return jsonify(advert_details)
        else:
            return {"error": "House does not exists"}, 404
    except CandidateException as e:
        return jsonify({"error": str(e)}), 500


@app.route('/houses/<int:idAnnonce>/requests', methods=['POST'])
def create_request(idAnnonce: str):
    """
    Ajoute une nouvelle candidature à une annonce
    """
    user_data = getJwtData()
    user_id = user_data.id
    # add error handling if message is not provided
    message = request.json.get("message")
    if not message:
        return jsonify({"error": "property [message] is required in body"}), 400
    try:
        id_candidature = service.create_request(idAnnonce, message, user_id)
        return jsonify({
            "message": "Candidature créée avec succès",
            "requestId": id_candidature
        }), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    startMicroservice(app)
