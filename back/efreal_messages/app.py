from flask import request, jsonify
from efreal_messages import message_REPO
from efreal_messages.exceptions import MessageException
from efreal_shared.main import createMicroservice, startMicroservice, useMySQL

app = createMicroservice('messages', __name__)
db = useMySQL()

@app.route('/houses/<int:idAnnonce>', methods=['GET'])
def getAllMessagesFromAnnonce(idAnnonce: int):
    """
    Affiche tous les groupes de message d'une annonce
    """
    result = []
    for row in message_REPO.fetchallCandidatureFromAnnonce(id=idAnnonce, db=db):
        result.append({
            "Id": row[0],
            "IdAnnonce": row[1],
            "IdProspect": row[2],
            "Statut": row[3]
        })
    return jsonify({"requests": result})


@app.route('/requests/<int:idCandidature>', methods=['GET'])
def getAllMessagesFromCandidature(idCandidature):
    """
    Ajoute un message dans une conversation
    """
    result = []
    for row in message_REPO.fetchallMessageFromCandidature(id=idCandidature, db=db):
        result.append({
            "Id": row[0],
            "IsOwner": row[1],
            "Date": row[2].isoformat(),
            "Message": row[3]
        })

    return jsonify({"messages": result})


@app.route('/requests/<int:idCandidature>', methods=['POST'])
def sendMessage(idCandidature):
    """
    Ajoute un message dans une conversation
    """
    IsOwner = request.json["IsOwner"]
    Message = request.json["Message"]
    try:
        message_id = message_REPO.insertMessage(
            db=db, idCandidature=idCandidature, IsOwner=IsOwner, Message=Message
        )
        return jsonify({"status": "success", "id": message_id}), 201
    except MessageException as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    startMicroservice(app)
    