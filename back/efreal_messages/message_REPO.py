import mysql.connector

from efreal_messages.exceptions import MessageException
from datetime import datetime


def fetchallCandidatureFromAnnonce(id : int, db: mysql.connector.MySQLConnection):
    with db.cursor() as c:
        c.execute("""
            SELECT Id, IdAnnonce, IdProspect, Statut FROM Candidature
            WHERE IdAnnonce = %s
        """, (id,))
        result = c.fetchall()
        return result
    
def fetchallMessageFromCandidature(id:int, db):
    with db.cursor() as c:
        c.execute("""
            SELECT Id, IsOwner, SentDate, Message FROM Message 
            WHERE IdCandidature = %s
            ORDER BY SentDate ASC
        """, (id,))
        result = c.fetchall()
        c.close()
        return result
    
def insertMessage(db, idCandidature, IsOwner, Message):
    with db.cursor() as c:
        try:
            c.execute("""
            INSERT INTO `Message` (`IdCandidature`, `IsOwner`, `SentDate`, `Message`)
                VALUES (%s, %s, %s, %s);
            """, (idCandidature, IsOwner, datetime.now(), Message))
            db.commit()
            return c.lastrowid
        except Exception as e:
            raise MessageException(e)