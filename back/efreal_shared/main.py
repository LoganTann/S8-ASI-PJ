from flask import Blueprint, Flask, abort, g, request
import jwt
import mysql.connector

from efreal_shared.DTO.JwtData import JwtData
from efreal_shared.User.UserService import UserService

print("Creating server...")
app = Flask(__name__)

print("Connecting to database...")
db = mysql.connector.connect(
    host="mysql",
    user="toto",
    password="mobile",
    database="efreal_estate",
    charset='utf8',
    use_unicode=True
)
db.autocommit = True
current_module = "unknown"

userService = UserService(db)

@app.route('/')
def ping():
    """
    Affiche les statuts du serveur
    """
    return {
        "service": current_module
    }

@app.before_request
def parse_jwt():
    auth_header = request.headers.get('Authorization')
    if not auth_header:
        abort(403, "Missing JWT token in the Authorization header")
        
    token = auth_header.split(' ')[1]
    try:
        decoded_token = jwt.decode(token, verify=False, algorithms=['RS384'], options={"verify_signature": False})
    except jwt.DecodeError as e:
        abort(403, "JWT token is invalid \n" + str(e) )
    
    g.id = decoded_token.get('id')
    g.username = decoded_token.get('username')
    g.role = decoded_token.get('role')
    if (g.id == None or g.username == None or g.role == None):
        abort(403, "JWT token content is invalid" + str(decoded_token));
    if (userService.isUserUnknown(g.id)):
        app.logger.info('Unknown user ' + str(g.id) + ', creating it...')
        userService.createEmptyUser(g.id, g.username);
        app.logger.info('User ' + str(g.id) + ' created.')


def getJwtData():
    return JwtData(
        g.id,
        g.username,
        g.role
    )

def useMySQL():
    return db


def createMicroservice(name: str, package: str):
    global current_module
    current_module = name
    return Blueprint(name, package)


def startMicroservice(module: Blueprint):
    print("Starting server...")
    with db:
        app.register_blueprint(module)
        app.run(host="0.0.0.0", debug=True)
