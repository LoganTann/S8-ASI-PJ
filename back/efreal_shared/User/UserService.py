import mysql.connector


class UserService:
    database: mysql.connector.MySQLConnection
    
    def __init__(self, database: mysql.connector.MySQLConnection):
        self.database = database

    def createEmptyUser(self, idUser: int, username):
        """
        Crée un utilisateur vide
        """
        with self.database.cursor() as cursor:
            sql = """
                INSERT INTO Profile(Id, FirstName, LastName, City) VALUES (%s, %s, %s, %s)
            """
            val = (idUser, username, "", "")
            cursor.execute(sql, val)
            self.database.commit()
            return cursor.lastrowid
            
    def isUserUnknown(self, idUser: int):
        """
        Vérifie si un utilisateur est inconnu
        """
        with self.database.cursor() as cursor:
            sql = """
                SELECT Id FROM Profile
                WHERE Id = %s
            """
            val = (idUser,)
            cursor.execute(sql, val)
            result = cursor.fetchone()
        return result is None
