

from dataclasses import dataclass


@dataclass
class JwtData:
    id: str = None
    firstname: str = None
    lastname: str = None