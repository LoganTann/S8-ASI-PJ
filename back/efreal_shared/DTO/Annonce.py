from dataclasses import dataclass


@dataclass
class Annonce():
    id: int = None
    type: str = None
    prix: float = None
    image: str = None
    ville: str = None
    description: str = None
    adresse: str = None
    nbpieces: int = None
    surface: float = None
    etat: str = None
    proprio: int = None
