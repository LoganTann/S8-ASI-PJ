class User():
    id: int = None
    firstname: str = None
    lastname: str = None
    city: str = None

    def __init__(self, id: int = None, firstname: str = None, lastname: str = None, city: str = None):
        self.id = id
        self.firstname = firstname
        self.lastname = lastname
        self.city = city
