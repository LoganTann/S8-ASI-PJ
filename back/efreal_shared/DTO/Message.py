from datetime import datetime
from dataclasses import dataclass


@dataclass
class Message():
    id: int = None
    idcandidature: int = None
    isowner: bool = None
    sentdate: datetime = None
    message: str = None
