from dataclasses import dataclass


@dataclass
class Candidature():
    id: int = None
    idannonce: int = None
    idprospect: int = None
    statut: str = None
