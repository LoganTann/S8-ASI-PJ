from flask import jsonify, request

from efreal_shared.main import createMicroservice, getJwtData, startMicroservice, useMySQL
from .proprio_repo import ProprioRepository
from .proprio_service import ProprioService
from efreal_shared.DTO.Annonce import Annonce

app = createMicroservice('proprio', __name__)
db = useMySQL()

repo = ProprioRepository(db)
service = ProprioService(repo)


@app.route('/houses', methods=['GET'])
def getMyHouses():
    """
    Donne les annonces créées par le propriétaire
    """
    jwt = getJwtData()
    userId = jwt.id
    try:
        adverts_details = service.getHousesofAProprio(userId)
        if adverts_details:
            return jsonify(adverts_details)
        else:
            return jsonify([])
    except Exception as e:
        return jsonify({"error": str(e)}), 500

@app.route('/houses', methods=['POST'])
def createHouse():
    """
    Créer une annonce logement
    """
    userData = getJwtData()
    
    annonce_info = Annonce(
        type = request.json["type"],
        prix = request.json["prix"],
        image = request.json["image"],
        ville = request.json["ville"],
        description = request.json["description"],
        adresse = request.json["adresse"],
        nbpieces = request.json["nbpieces"],
        surface = request.json["surface"],
        etat = request.json["etat"],
        proprio = userData.id
    )
    try:
        annonce_id = service.createHouse(annonce_info)
        return jsonify({
            "status": "success",
            "id": annonce_id
        }), 201
    except Exception as e:
        return jsonify({"error": str(e)}), 500

@app.route('/houses/<int:idAnnonce>/requests', methods=['GET'])
def getRequests(idAnnonce):
    """
    Récupère les candidatures créées sur un bien
    """
    try:
        requests_details = service.getRequestsofAHouses(idAnnonce)
        if requests_details:
            return jsonify(requests_details)
        else:
            return jsonify([])
    except Exception as e:
        return jsonify({"error": str(e)}), 500

@app.route('/houses/<int:idAnnonce>/requests/<int:idCandidature>/status', methods=['PATCH'])
def editRequestStatus(idAnnonce: int, idCandidature: int):
    """
    Change l'avancement d'une candidature à l'annonce

    pending -> visite -> accepté -> refused
    """
    status = request.json["status"]
    try:
        service.editRequestStatus(idCandidature, status)
        return jsonify({"message": "Statut changé avec succès"})
    except Exception as e:
        return jsonify({"error": str(e)}), 500


if __name__ == '__main__':
    startMicroservice(app)
    
