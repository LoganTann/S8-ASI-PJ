from .exceptions import ProprioException
from .proprio_repo import ProprioRepository
from efreal_shared.DTO.Annonce import Annonce
from efreal_shared.DTO.Candidature import Candidature


class ProprioService:
    def __init__(self, repo: ProprioRepository):
        self.repo = repo

    def getHousesofAProprio(self, id_proprio):
        try:
            result = []
            for row in self.repo.getHousesofAProprio(id_proprio):
                result.append(
                    Annonce(
                        id=row[0],
                        type=row[1],
                        prix=row[2],
                        image=row[3],
                        ville=row[4],
                        description=row[5],
                        adresse=row[6],
                        nbpieces=row[7],
                        surface=row[8],
                        etat=row[9],
                        proprio=id_proprio
                    )
                )
            return result
        except Exception as e:
            raise ProprioException(
                f"An error occured while getting informations of Houses {str(e)}")

    def createHouse(self, annonce_info):
        try:
            return self.repo.createHouse(annonce_info)
        except Exception as e:
            raise ProprioException(
                f"An error occured while creation of a House {str(e)}")

    def getRequestsofAHouses(self, id_house):
        try:
            result = []
            for row in self.repo.getRequests(id_house):
                result.append(
                    Candidature(
                        id = row[0],
                        idannonce = row[1],
                        idprospect = row[2],
                        statut = row[3]
                    )
                )
            return result
        except Exception as e:
            raise ProprioException(
                f"An error occured while getting informations of Houses {str(e)}")

    def editRequestStatus(self, id_request, status):
        try:
            self.repo.editStatus(status, id_request)
        except Exception as e:
            raise ProprioException(
                f"An error occured while updating a Status {str(e)}")
