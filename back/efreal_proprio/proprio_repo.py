import mysql.connector

class ProprioRepository:
    def __init__(self, db: mysql.connector.MySQLConnection):
        self.db = db

    def getHousesofAProprio(self, id_proprio):
        with self.db.cursor() as cursor:
            query = """
            SELECT Id, TypeAnnonce, Prix, Image, Ville, Description, Adresse, NombrePieces, SurfaceHabitable, Etat
            FROM Annonce WHERE IdProprietaire = %s
            """
            cursor.execute(query, (id_proprio,))
            return cursor.fetchall()

    def createHouse(self, annonce_info):
        with self.db.cursor() as cursor:
            query = """
            INSERT INTO Annonce (TypeAnnonce, Prix, Image, Ville, Description, Adresse, NombrePieces, SurfaceHabitable, Etat, IdProprietaire)
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """
            cursor.execute(query, (annonce_info.type, annonce_info.prix, annonce_info.image, annonce_info.ville, annonce_info.description,
                           annonce_info.adresse, annonce_info.nbpieces, annonce_info.surface, annonce_info.etat, annonce_info.proprio))
            self.db.commit()
            return cursor.lastrowid;    

    def getRequests(self, id_annonce):
        with self.db.cursor() as cursor:
            query = """
            SELECT Id, IdAnnonce, IdProspect, Statut
            FROM Candidature WHERE IdAnnonce = %s
            """
            cursor.execute(query, (id_annonce,))
            return cursor.fetchall()

    def getRequestStatus(self, Id):
        with self.db.cursor() as cursor:
            query = """
            SELECT Status
            FROM Candidature WHERE Id = %s
            """
            cursor.execute(query, (Id,))
            return cursor.fetchone()

    def editStatus(self, new_status, id_request):
        with self.db.cursor() as cursor:
            query = """
            UPDATE Candidature
            Set Statut = %s
            WHERE Id = %s
            """
            cursor.execute(query, (new_status, id_request))
            self.db.commit()
            return cursor.fetchall()
