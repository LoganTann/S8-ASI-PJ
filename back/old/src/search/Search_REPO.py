
def listHomes(db, sqlParams):
    with db.cursor() as c:
        query = """
            SELECT L.Id, L.Type, L.NombrePieces, L.SurfaceHabitable, L.Ville,
                A.Image, A.TypeAnnonce, A.Prix, A.Date
            FROM Annonce A
                INNER JOIN Logement L on A.IdLogement = L.Id
            WHERE L.Ville LIKE %s
              AND L.Etat LIKE %s
              AND L.Type LIKE %s
              AND (A.Prix BETWEEN %s AND %s)
        """
        execParams = (
            sqlParams["ville"],
            sqlParams["etat"],
            sqlParams["typeLogement"],
            sqlParams["prixMin"], sqlParams["prixMax"]
        )
        c.execute(query, execParams)
        return c.fetchall()

def listCities(db, cityInput):
    with db.cursor() as c:
        if (cityInput == ""):
            c.execute("""
                SELECT DISTINCT L.Ville
                FROM Logement L
                    RIGHT JOIN Annonce A ON L.Id = A.IdLogement
                ORDER BY L.Ville
            """)
            return c.fetchall()
        
        params = (cityInput+"%",)
        c.execute("""
            SELECT DISTINCT L.Ville
            FROM Logement L
                RIGHT JOIN Annonce A ON L.Id = A.IdLogement
            WHERE L.Ville LIKE %s
        """, params)
        return c.fetchall()