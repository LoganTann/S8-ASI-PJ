from src.report import Report_REPO

def listCandidateWithMoreThanThreeApplication(db):
    result = []
    for row in Report_REPO.ListCandidateWithMoreThanThreeApplication(db):
        result.append({
            "nom": row[0],
            "prenom": row[1],
            "nbCandidatures": row[2]
        })
    return result


def listHomesWithoutApplication(db):
    result = []
    for row in Report_REPO.listHomesWithoutApplication(db):
        result.append({
            "typeLogement": row[0],
            "adresse": row[1],
            "surfaceHabitable": row[2],
            "ville": row[3],
            "typeAnnonce": ("Vente" if row[4] == "V" else "Location"),
            "prix": row[5],
            "dateDispo": row[6].strftime("%d/%m/%Y")
        })
    return result

def listCityOwners(db, ville):
    result = []
    for row in Report_REPO.ListCityOwners(db, ville):
        result.append({
            "nom": row[0],
            "prenom": row[1],
            "type": row[2],
            "adresse": row[3],
            "surfaceHabitable": row[4]
        })
    return result

def turnover(db):
    rows = Report_REPO.Turnover(db)
    row = rows[0]
    return {
        "nbLogements": row[0],
        "nbAnnonces": row[1],
        "nbCandidatures": row[2],
        "chiffreAffaire": row[3]
    }

def apartmentMoreExpensiveThanHouses(db):
    result = []
    for row in Report_REPO.apartmentMoreExpensiveThanHouses(db):
        result.append({
            "type": "Location" if row[0] == "L" else "Vente",
            "ville": row[1],
            "adresse": row[2],
            "surfaceHabitable": row[3],
            "nombrePieces": row[4],
            "prix": row[5]
        })
    return result

def announcementWithoutApplication(db):
    result = []
    for row in Report_REPO.announcementWithoutApplication(db):
        result.append({
            "typeAnnonce": row[0],
            "prix": row[1],
            "ville": row[2],
            "surfaceHabitable": row[3]
        })
    return result

def notOwner(db):
    result = []
    for row in Report_REPO.memberNotOwner(db):
        result.append({
            "nom": row[0],
            "prenom": row[1]
        })
    return result


