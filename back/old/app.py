from flask import Flask, render_template, request
import mysql.connector

from src.house import House_SVC
from src.owner import Owner_SVC
from src.search import Search_SVC
from src.report import Report_SVC
from src.admin import Admin_SVC
from src.User.UserService import UserService

app = Flask(
    __name__,
    static_url_path='', 
    template_folder='static', 
    static_folder='static'
)

db = mysql.connector.connect(
  host="mysql",
  user="toto",
  password="mobile",
  database="efreal_estate"
)


def resetDB():
    """
    Réintialise la BDD à chaque démarrage
    Assume que la base "efreal_estate" a été créée (car python crash si la connexion échoue)
    """
    with open("efreal_estate.sql","r") as file:
        toExec = file.read()
        data = ""
        with db.cursor() as c:
            toExec = "START TRANSACTION;\n" + toExec + "\nCOMMIT;\n"
            c.execute(toExec)
            data = c.fetchall()
            print("🟢 Base de données réintialisée")
        db.reconnect()
        return {"toExec": toExec, "data": data}

@app.after_request
def allowLocalhostCros(response):
    response.headers["Access-Control-Allow-Origin"] = "http://localhost:5173"
    return response


@app.route('/')
def index():
    return render_template('index.html')

# -- Affichage et recherche d'annonces ----------

@app.route('/search', methods=['GET'])
def searchHouse():
    """
    Recherche des annonces
    """
    queryParams = request.args
    sqlParams = {
        "ville": queryParams.get("ville", default='%'),
        "etat": queryParams.get("etat", default='%'),
        "typeLogement": queryParams.get("typeLogement", default='%'),
        "prixMin": queryParams.get("prixMin", default=0),
        "prixMax": queryParams.get("prixMax", default=999_999_999_999)
    }
    return Search_SVC.listHomes(db, sqlParams)

@app.route('/search/cities', methods=['GET'])
@app.route('/search/cities/<name>', methods=['GET'])
def listCities(name = ""):
    """
    Autocomplète des villes
    """
    return Search_SVC.listCities(db, name)

# -- Espace candidat ----------

@app.route('/api/house/<int:idAnnonce>')
def getHouse(idAnnonce):
    """
    Donne les détails d'une annonce
    """
    return {"TODO": idAnnonce}

@app.route('/api/house/', methods=['POST'])
def createHouse():
    """
    Créer un logement
    """
    return {"TODO": "TODO"}

@app.route('/api/house/<int:idAnnonce>/requests', methods=['POST'])
def createRequest(idAnnonce):
    """
    Crée une candidature (lie une id annonce à un id utilisateur)
    Initialise un premier message
    """
    return {"TODO": "TODO"}

# -- Espace Propriétaire ----------

@app.route('/api/possession/<int:idPersonne>')
def owner(idPersonne):
    return Owner_SVC.listHousesByOwner(db, idPersonne)

# -- Espace Agence ----------------

@app.route('/api/admin/allApplication')
def showAllApplication():
    queryParams = request.args
    sqlParams = {
        "idAnnonce": queryParams.get("idAnnonce", default=0)
    }
    return Admin_SVC.showAllApplication(db, sqlParams)

@app.route('/api/application/<int:idApplication>')
def application(idApplication):
    return Admin_SVC.showApplicationDetails(db, idApplication)


@app.route('/api/admin/allAppointments')
def adminVisitRequests():
    return Admin_SVC.listVisitRequests(db)


# -- Reporting ----------------

@app.route('/api/report/desperateCandidates')
def reportDesperateCandidates():
    return Report_SVC.listCandidateWithMoreThanThreeApplication(db)

@app.route('/api/report/noApplication')
def reportNoApplication():
    return Report_SVC.listHomesWithoutApplication(db)

@app.route('/api/report/expensiveApartment')
def apartmentMoreExpensiveThanHouses():
    return Report_SVC.apartmentMoreExpensiveThanHouses(db)

@app.route('/api/report/richPeople/<ville>')
def reportRichPeople(ville):
    return Report_SVC.listCityOwners(db, ville)

@app.route('/api/report/chiffreAffaire')
def reportChiffreAffaire():
    return Report_SVC.turnover(db)

@app.route('/api/report/uglyHouses')
def reportAnnouncementWithoutApplication():
    return Report_SVC.announcementWithoutApplication(db)

@app.route('/api/report/notOwner')
def memberNotOwner():
    return Report_SVC.notOwner(db)

@app.route('/api/forceReset')
def forceReset():
    return resetDB()

if __name__ == "__main__":
    with db:
        resetDB()
        app.run(host="0.0.0.0", debug=True)
