# S8-ArchitectureSI-PJ
coucou
## Liste des microservices

- [Gateway](nginx/default.conf)
- [Authentification](auth/) - `docker compose up auth --build`
    - Documentation : [auth/readme.md](auth/readme.md)
- [Candidate](back/efreal_candidate/)
- [Proprio](back/efreal_proprio/)
- [Messages](back/efreal_messages/)
- [Front-end](front/)

## Développement

1. Installez WSL 2 et docker
2. Ouvrez le workspace sur vscode [S8-ArchitectureSI-PJ.code-workspace]
3. Installez les extensions suggérées
4. Démarrez le microservice que vous souhaitez utiliser. Ou `docker compose up --build` pour tous les services.

coucou