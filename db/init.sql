START TRANSACTION;

SET NAMES utf8;

DROP TABLE IF EXISTS `Message`;
DROP TABLE IF EXISTS `Candidature`;
DROP TABLE IF EXISTS `Annonce`;
DROP TABLE IF EXISTS `Profile`;

CREATE TABLE `Profile` (
  `Id` int NOT NULL, -- id utilisateur
  `FirstName` varchar(150) NOT NULL, -- ex: Logan
  `LastName` varchar(150) NOT NULL, -- ex: TANN
  `City` text NOT NULL, -- ex: Paris
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Annonce` (
  `Id` int NOT NULL AUTO_INCREMENT, -- ex: 1
  `TypeAnnonce` varchar(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL, -- ex: L
  `Prix` decimal(10,0) NOT NULL, -- ex: 800.5
  `Image` varchar(100) NOT NULL, -- ex: /images/1_appartement.png
  `Ville` varchar(60) NOT NULL,
  `Description` text NOT NULL,
  `Adresse` text NOT NULL,
  `NombrePieces` int NOT NULL,
  `SurfaceHabitable` decimal(10,0) NOT NULL,
  `Etat` varchar(20) NOT NULL,
  `IdProprietaire` int NOT NULL,
  PRIMARY KEY (`Id`),
  FOREIGN KEY (`IdProprietaire`) REFERENCES `Profile` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Candidature` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IdAnnonce` int NOT NULL, -- ex: 1
  `IdProspect` int NOT NULL, -- ex: 2
  `Statut` text NOT NULL, 
  PRIMARY KEY (`Id`),
  FOREIGN KEY (`IdAnnonce`) REFERENCES `Annonce` (`Id`) ON DELETE CASCADE,
  FOREIGN KEY (`IdProspect`) REFERENCES `Profile` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Message` (
  `Id` int NOT NULL AUTO_INCREMENT,
  `IdCandidature` int NOT NULL,
  `IsOwner` TINYINT NOT NULL, -- 0 : candidate -- 1 : Owner
  `SentDate` DATETIME NOT NULL,
  `Message` text NOT NULL,
  PRIMARY KEY (`Id`),
  FOREIGN KEY (`IdCandidature`) REFERENCES `Candidature` (`Id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Peuplement des tables
INSERT INTO Profile VALUES (1, 'Yasmine', 'AZIB', 'Paris');
INSERT INTO Profile VALUES (2, 'Logan', 'Tann', 'Paris');

INSERT INTO Annonce (TypeAnnonce, Prix, Image, Ville, Description, Adresse, NombrePieces, SurfaceHabitable, Etat, IdProprietaire)
VALUES
('L', 800.5, '/images/1_appartement.png', 'Paris', 'Bel appartement en centre-ville', '123 Rue des Roses', 3, 70, 'Bon état', 1),
('V', 120000, '/images/2_maison.png', 'Lyon', 'Charmante maison avec jardin', '456 Avenue du Soleil', 5, 150, 'Excellent état', 2);

INSERT INTO Candidature (IdAnnonce, IdProspect, Statut)
VALUES
(1, 1, 'Pending'),
(2, 2, 'Accepted');

INSERT INTO Message (IdCandidature, IsOwner, SentDate, Message)
VALUES
(1, 0, '2024-05-17', 'Bonjour, je suis intéressé'),
(1, 1, '2024-05-18', 'Bonjour, je vous remercie pour votre intérêt pour ma maison.');

COMMIT;