import axios from "axios";

/**
 * Préfixe la chaine de caractère par "/api"
 */
export function apiEndpoint(suffix) {
    const prefix = location.href.startsWith("http://localhost:5173/")
        ? "http://localhost:5000/api"
        : "/api";
    return prefix.concat(suffix);
}

export async function searchCity(queryString, showResults) {
    const toSearch = queryString
        ? `/search/cities/${queryString}`
        : "/search/cities";
    const { data } = await axios.get(apiEndpoint(toSearch));
    showResults(data);
}

export { axios };
