import { defineStore } from "pinia";

export const useLoginStore = defineStore("login", {
    state: () => ({
        userId: 0,
    }),
    getters: {
        isAdmin: (state) => state.userId < 0,
        isLogged: (state) => state.userId != 0,
    },
    actions: {
        login(id, pass) {
            const key = `${id}+${pass}`;
            const ids = {
                "admin+root": -1,
                "logan.tann+efrei": 1,
                "root+root": 1,
                "charlotte.rousseau+efrei": 10,
            };
            const result = ids[key];
            if (result) {
                this.userId = result;
                return true;
            }
            return false;
        },
        disconnect() {
            this.userId = 0;
        },
    },
});
