import { createApp } from "vue";
import { createPinia } from "pinia";
import router from "./router";

import "element-plus/dist/index.css";
const pinia = createPinia();

import App from "./App.vue";
const app = createApp(App);

app.use(pinia);
app.use(router);

app.mount("#app");
