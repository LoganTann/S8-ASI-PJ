import { createRouter, createWebHistory } from "vue-router";
import HomePage from "../components/HomePage.vue";
import ReportNoApplications from "../components/pages/admin/ReportNoApplications.vue";
import ReportDesperateCandidates from "../components/pages/admin/ReportDesperateCandidates.vue";
import ReportExpensiveApartment from "../components/pages/admin/ReportExpensiveApartment.vue";
import ReportOwnersByCity from "../components/pages/admin/ReportOwnersByCity.vue";
import ReportNotOwners from "../components/pages/admin/ReportNotOwners.vue";
import AdminIndex from "../components/pages/admin/AdminIndex.vue";
import Applications from "../components/pages/admin/Applications.vue";
import Appointments from "../components/pages/admin/Appointments.vue";
import OwnerPage from "../components/pages/OwnerPage.vue";

const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        { path: "/", component: HomePage },
        { path: "/house/:id", component: HomePage },
        { path: "/owner", component: OwnerPage },
        { path: "/admin", component: AdminIndex },
        { path: "/admin/applications", component: Applications },
        { path: "/admin/appointments", component: Appointments },
        {
            path: "/admin/report/expensives",
            component: ReportExpensiveApartment,
        },
        { path: "/admin/report/richPeople", component: ReportOwnersByCity },
        { path: "/admin/report/notOwners", component: ReportNotOwners },
        {
            path: "/admin/report/desperateCandidates",
            component: ReportDesperateCandidates,
        },
        { path: "/admin/report/uglyHouses", component: ReportNoApplications },
    ],
});

export default router;
