import { EntityManager } from '../persistance/EntityManager.ts';
import * as bcrypt from 'https://deno.land/x/bcrypt@v0.4.1/mod.ts';
import { Role } from '../types/role.ts';
import { JwtService } from './JwtService.ts';

export class UserService {
    public static async createUser(username: string, password: string, role: Role) {
        const [id] = crypto.getRandomValues(new Uint8Array(1));
        const passwordHashed = await bcrypt.hash(password);
        EntityManager.instance.user.createUser({
            id,
            username,
            password: passwordHashed,
            role,
        });
        return {
            id,
            username,
            role,
        };
    }
    public static async loginUser(username: string, password: string) {
        const foundUser = EntityManager.instance.user.readUser(username);
        if (!foundUser) {
            throw new Error('Username does not exists');
        }
        const isPasswordValid = await bcrypt.compare(password, foundUser.password);
        if (!isPasswordValid) {
            throw new Error('Password is incorrect');
        }
        return await JwtService.sign({
            id: foundUser.id,
            username: foundUser.username,
            role: foundUser.role,
        });
    }
}
