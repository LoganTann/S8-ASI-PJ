import { create } from 'https://deno.land/x/djwt@v3.0.2/mod.ts';

const { privateKey, publicKey } = await crypto.subtle.generateKey(
    {
        name: 'RSASSA-PKCS1-v1_5',
        modulusLength: 4096,
        publicExponent: new Uint8Array([1, 0, 1]),
        hash: 'SHA-384',
    },
    true,
    ['verify', 'sign'],
);

export class JwtService {
    public static async sign(payload: Record<string, unknown>) {
        return await create({ alg: 'RS384', typ: 'JWT' }, payload, privateKey);
    }

    public static async getPublicKey() {
        return await crypto.subtle.exportKey('jwk', publicKey);
    }
}
