const url = 'http://localhost:5001';
try {
    const response = await fetch(url);
    if (response.status === 200) {
        Deno.exit(0);
    } else {
        Deno.exit(1);
    }
} catch (_error) {
    Deno.exit(1);
}
