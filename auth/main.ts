import { EntityManager } from './persistance/EntityManager.ts';
import { Hono } from 'https://deno.land/x/hono@v4.3.4/mod.ts';
import { UsersController } from './routes/UsersController.ts';
import { LoginController } from './routes/LoginController.ts';
import { SecurityController } from './routes/SecurityController.ts';

const PORT = 5001;

function main() {
    resetApp();
    setupRestAPI();
}

function resetApp() {
    EntityManager.instance.down().up();
}
function setupRestAPI() {
    const app = new Hono();
    app
        .get('/', (c) => {
            return c.json({ service: 'auth' });
        })
        .route('/users', UsersController)
        .route('/login', LoginController)
        .route('/security', SecurityController);

    Deno.serve({
        port: PORT,
        onListen() {
            console.log('Authentication microservice is up in port', PORT);
        },
    }, app.fetch);
}

main();
