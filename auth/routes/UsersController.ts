import { Hono } from 'https://deno.land/x/hono@v4.3.4/mod.ts';
import { checkRole, Role } from '../types/role.ts';
import { UserService } from '../services/UserService.ts';

export const UsersController = new Hono();

UsersController.post('/', async (c) => {
    const body = await c.req.json();
    if (!body.username || !body.password) {
        return c.json({
            status: 'error',
            message: 'Missing username and password field',
        }, 400);
    }
    if (!checkRole(body.role)) {
        body.role = Role.Candidate;
    }
    try {
        const createdUser = await UserService.createUser(body.username, body.password, body.role);
        return c.json({
            status: 'success',
            user: createdUser,
        });
    } catch (e) {
        return c.json({
            status: 'error',
            message: e.message,
        }, 400);
    }
});
