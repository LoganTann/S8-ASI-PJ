import { Hono } from 'https://deno.land/x/hono@v4.3.4/mod.ts';
import { JwtService } from '../services/JwtService.ts';

export const SecurityController = new Hono();

SecurityController.get('/public-key', async (c) => {
    try {
        return c.json({
            status: 'success',
            jwk: [
                await JwtService.getPublicKey(),
            ],
        });
    } catch (error) {
        return c.json({
            status: 'error',
            message: error.message,
        }, 500);
    }
});
