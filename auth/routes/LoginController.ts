import { Hono } from 'https://deno.land/x/hono@v4.3.4/mod.ts';
import { UserService } from '../services/UserService.ts';

export const LoginController = new Hono();

LoginController.post('/', async (c) => {
    const { username, password } = await c.req.json();

    if (!username || !password) {
        return c.json({
            status: 'error',
            message: 'Username and password are required',
        }, 400);
    }
    try {
        const jwt = await UserService.loginUser(username, password);
        return c.json({
            status: 'success',
            token: jwt,
        });
    } catch (e) {
        return c.json({
            status: 'error',
            message: e.message,
        }, 401);
    }
});
