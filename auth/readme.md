# Microservice d'authentification - Projet Architecture SI

Langage : Typescript avec Deno

Port : 5001

Démarrer le service : `docker compose up auth --build`

## Créer un utilisateur

```js
// POST localhost:5001/users
{
    "username": "toto",
    "password": "test",
    "role": "candidate" // or "owner" or "admin"
}

// Response
{
    "status": "success",
    "user": {
        "id": 346569764,
        "username": "toto",
        "role": "candidate"
    }
}
```

## Se connecter

```js
// POST localhost:5001/users
{
    "username": "toto.mobile",
    "password": "patate"
}
// Response
{
    "status": "success",
    "token": "eyJhbGciOiJSUzM4NCIsInR5cCI6IkpXVCJ9.eyJpZCI6NjM1MjUwMzUwLCJ1c2VybmFtZSI6InRvdG8iLCJyb2xlIjoiY2FuZGlkYXRlIn0.J4lM8ZdepbsJh9Pa1sKcWgYAmwjdqRNPR4O47jbogwHz0-Sp5WOfaczGZM04OOpLc1u5He_zUCvLGRp3-69eo8128Zt3IIe8g947oxrFS_29lUk5KFYeMGMpbxuDRcrQZ7PZsKxG-NYu29a3Owh6RqJbpfjGC35mttbHhjlR8CdxHUdTogKe0h9aGgQUMZ0SovHyyvSk5mmwJAA1iwap7V05R0LCwEi0EQr7dYmwhKwkbaDSZqK5d5YVL-5ksxsCDP2eFHR4iwHavkkR-cEtxcQm3YYwlws-yQvW9wKwdVo0tTChnhi0xUg34xKtUH4dtW8Z2dgnEHWYc7l36w07KLd6WGldUE7Z3W8rIbOUyBVG2o7wxxgvaD2Tpl7twBf08F-AmnrFR6xmH5znwgJNU5_keX_EmW7nJlchiRpXbAARpFCHL3VgJ4ekPJ5BDDbkaPegK2xSlXwtlg95woejVHDxdXSJdU3hUW5Al9BIH5jq5RQXUAMtVAYb9RqeHN08kk80wY6LHDvUYjN_vxC6dvVJKFGLRtSJJ1eIlL64aF-GsDGkMiFRfMb-OylveshqQN67UHenmnzplKYVoXLkUxReWjBIokjJPUU9Ftarqcmz3J3mSRBkfO-EcI-v-kH5Lc1qSZgBVCbHxuDVfc29EByO9HL2rLoczrcixx-NVTw"
}
```

## Récupérer la clé publique pour vérifier la signature

```js
// GET localhost:5001/security/public-key

// Response : 
{
  "status": "success",
  "key": {
    "kty": "RSA",
    "alg": "RS384",
    "n": "5UEik4gC2plQIMxPzhj77dHQ1ztNXMK8jQDSHUdVOV6Yclsn134S5X9ocpIbSmqf-xme-fCfZRblFKScwRXLGxh5OSMTBDp6iGoInNOuwih1F67Rwq0mdoPm1uqeMOq_cW73jlUOPxdt3LQzozZhHYeJkHT_0lBgphd0c-VWj90y4rAbJ0LWRdoQxRSowDbe_sFbQ6ggjldWToeiLd-nSDXndeiE6yEr4GZ9bFcv21mGStchG1U4SPFOPwu-B54J3BJSaLOH8fwv1smdatlqSlJrMc1tvH0DRtnoTAMOipHVLy8tDmnK81iNSOwtMAG6JmJYIGI4v09s1_lSaaLONYiR9pteQGDIbFRbFG8070LH1VV1dELVY1QXH4x0e85twgdVOKABUsWFeQOUPgDX96u-KOvz-0UkezWJp76TID12TntNt3G6pYdbSsqBYszoEzweeq3alKBhR4vGVX7Z6buDn-WZgatNn8Remd98VQVLUOWtGPTtgmIoXmx0CY3-SAB6ydZvdwAsCM8wFhifrcRwur8BedZxSYU95B85UfTFFbFxjoNAcAp1VfhTIMOJI6MZ6HULqQcgrkObremZV7bOV86wF-ohxRlDOLHoBhqfk0zJHqFEOUYEilkAbLPzh9nRU8cWNYvb9iHYuNmGk4DibIxpy60aSufu8sXI838",
    "e": "AQAB",
    "key_ops": [
      "verify"
    ],
    "ext": true
  }
}
```
