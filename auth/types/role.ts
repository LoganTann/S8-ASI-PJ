export enum Role {
    Candidate = 'candidate',
    Owner = 'owner',
    Administrator = 'admin',
}

export function checkRole(role: string): role is Role {
    return [Role.Candidate, Role.Owner, Role.Administrator].indexOf(role as Role) !== -1;
}
export function assertRole(role: string): asserts role is Role {
    if (!checkRole(role)) {
        throw new Error('Invalid role. Expected one of: candidate, owner, admin.');
    }
}
