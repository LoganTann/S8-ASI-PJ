import { DB } from 'https://deno.land/x/sqlite@v3.8/mod.ts';
import { UserRepository } from './UserRepository.ts';

export class EntityManager {
    public readonly user: UserRepository;

    private db: DB;
    private constructor(dbName: string) {
        this.db = new DB(dbName);
        this.user = new UserRepository(this.db);
    }

    private static _instance: EntityManager;
    public static get instance() {
        if (!EntityManager._instance) {
            EntityManager._instance = new EntityManager('auth.db');
        }
        return EntityManager._instance;
    }

    public up() {
        this.user.up();
        return this;
    }

    public down() {
        this.user.down();
        return this;
    }
}
