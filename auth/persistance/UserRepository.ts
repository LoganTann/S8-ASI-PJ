import { DB } from 'https://deno.land/x/sqlite@v3.8/mod.ts';
import { Role } from '../types/role.ts';

export class UserRepository {
    public constructor(
        private readonly db: DB,
    ) {}

    public up() {
        this.db.query(`
        CREATE TABLE IF NOT EXISTS user (
            id INTEGER PRIMARY KEY,
            username TEXT,
            password TEXT,
            role TEXT,
            CONSTRAINT unique_username UNIQUE (username)
        )
        `);
    }
    public down() {
        this.db.query(`DROP TABLE IF EXISTS user;`);
    }

    public createUser(user: UserEntity) {
        this.db.query(
            `INSERT INTO user (id, username, password, role)
            VALUES (?, ?, ?, ?)
      `,
            [user.id, user.username, user.password, user.role],
        );
    }

    public readUser(username: string) {
        const rows = this.db.queryEntries<{
            id: string;
            username: string;
            password: string;
            role: Role;
        }>(
            `SELECT id, username, password, role FROM user WHERE username = :username`,
            { username },
        );
        return rows.at(0);
    }
}

export interface UserEntity {
    id: number;
    username: string;
    password: string;
    role: Role;
}
